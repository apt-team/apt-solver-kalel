/*
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * Copyright (C) 2012-2018 Julian Andres Klode <jak@jak-linux.org>
 * Copyright (C) 2018-2019 Canonical Ltd
 *
 * Some portions included below are taken from the dummy apt solver, and hence
 * share its copyright owners.
 *
 */
#include <clasp/config.h>
#include <apt-pkg/algorithms.h>
#include <apt-pkg/cachefile.h>
#include <apt-pkg/aptconfiguration.h>
#include <apt-pkg/configuration.h>
#include <apt-pkg/pkgsystem.h>
#include <apt-pkg/pkgcache.h>
#include <apt-pkg/policy.h>
#include <iostream>
#include <memory>
#include <queue>
#include <set>
#include <unordered_set>
#include <clasp/clasp_facade.h>
#include <clasp/literal.h>

#if 0
struct WeightedLit {
	int literal;
	int weight;
};

typedef std::vector<WeightedLit> WeightedVec;
#else
typedef Clasp::WeightLiteral WeightedLit;
typedef Clasp::PodVector<WeightedLit>::type WeightedVec;
#endif

enum class Op
{
   GE,
   EQ
};

struct Rule
{
   WeightedVec lhs;
   Op op;
   Clasp::weight_t rhs;
};

struct CompareProviders
{
   pkgCache::PkgIterator const Pkg;
   pkgCache &Cache;
   explicit CompareProviders(pkgCache::DepIterator const &Dep) : Pkg(Dep.TargetPkg()), Cache(*Dep.Cache()){};
   //bool operator() (APT::VersionList::iterator const &AV, APT::VersionList::iterator const &BV)
   bool operator()(pkgCache::Version *AV_, pkgCache::Version *BV_)
   {
      return reversed(BV_, AV_);
   }
   bool reversed(pkgCache::Version *AV_, pkgCache::Version *BV_)
   {
      if (AV_ == nullptr)
	 return false;
      if (BV_ == nullptr)
	 return true;
      pkgCache::VerIterator AV(Cache, AV_);
      pkgCache::VerIterator BV(Cache, BV_);
      pkgCache::PkgIterator const A = AV.ParentPkg();
      pkgCache::PkgIterator const B = BV.ParentPkg();

      // Prefer MA:same packages if other architectures for it are installed
      if ((AV->MultiArch & pkgCache::Version::Same) == pkgCache::Version::Same ||
	  (BV->MultiArch & pkgCache::Version::Same) == pkgCache::Version::Same)
      {
	 bool instA = false;
	 if ((AV->MultiArch & pkgCache::Version::Same) == pkgCache::Version::Same)
	 {
	    pkgCache::GrpIterator Grp = A.Group();
	    for (pkgCache::PkgIterator P = Grp.PackageList(); P.end() == false; P = Grp.NextPkg(P))
	       if (P->CurrentVer != 0)
	       {
		  instA = true;
		  break;
	       }
	 }
	 bool instB = false;
	 if ((BV->MultiArch & pkgCache::Version::Same) == pkgCache::Version::Same)
	 {
	    pkgCache::GrpIterator Grp = B.Group();
	    for (pkgCache::PkgIterator P = Grp.PackageList(); P.end() == false; P = Grp.NextPkg(P))
	    {
	       if (P->CurrentVer != 0)
	       {
		  instB = true;
		  break;
	       }
	    }
	 }
	 if (instA != instB)
	    return instA == false;
      }
      if ((A->CurrentVer == 0 || B->CurrentVer == 0) && A->CurrentVer != B->CurrentVer)
	 return A->CurrentVer == 0;
      // Prefer packages in the same group as the target; e.g. foo:i386, foo:amd64
      if (A->Group != B->Group)
      {
	 if (A->Group == Pkg->Group && B->Group != Pkg->Group)
	    return false;
	 else if (B->Group == Pkg->Group && A->Group != Pkg->Group)
	    return true;
      }
      // we like essentials
      if ((A->Flags & pkgCache::Flag::Essential) != (B->Flags & pkgCache::Flag::Essential))
      {
	 if ((A->Flags & pkgCache::Flag::Essential) == pkgCache::Flag::Essential)
	    return false;
	 else if ((B->Flags & pkgCache::Flag::Essential) == pkgCache::Flag::Essential)
	    return true;
      }
      if ((A->Flags & pkgCache::Flag::Important) != (B->Flags & pkgCache::Flag::Important))
      {
	 if ((A->Flags & pkgCache::Flag::Important) == pkgCache::Flag::Important)
	    return false;
	 else if ((B->Flags & pkgCache::Flag::Important) == pkgCache::Flag::Important)
	    return true;
      }
      // prefer native architecture
      if (strcmp(A.Arch(), B.Arch()) != 0)
      {
	 if (strcmp(A.Arch(), A.Cache()->NativeArch()) == 0)
	    return false;
	 else if (strcmp(B.Arch(), B.Cache()->NativeArch()) == 0)
	    return true;
	 std::vector<std::string> archs = APT::Configuration::getArchitectures();
	 for (std::vector<std::string>::const_iterator a = archs.begin(); a != archs.end(); ++a)
	    if (*a == A.Arch())
	       return false;
	    else if (*a == B.Arch())
	       return true;
      }
      // higher priority seems like a good idea
      if (AV->Priority != BV->Priority)
	 return AV->Priority > BV->Priority;
      // unable to decide…
      return A->ID < B->ID;
   }
};

struct Solver
{
   pkgCacheFile &cache;
   std::vector<Rule> rules;
   std::unique_ptr<int[]> literals;
   std::unique_ptr<int[]> pLiterals;
   std::unique_ptr<int[]> scores;
   std::vector<int> relaxLiterals;
   std::unique_ptr<pkgCache::Version *[]> literalsReverse;
   int literalCount = 0;
   std::set<pkgCache::Version *> versions;
   WeightedVec objective;

   Solver(pkgCacheFile &cache) : cache(cache), literals(new int[cache->Head().VersionCount]), pLiterals(new int[cache->Head().PackageCount]), scores(new int[cache->Head().PackageCount]), literalsReverse(new pkgCache::Version *[cache->Head().VersionCount])
   {
      for (int i = 0; i < cache->Head().VersionCount; i++)
      {
	 literals[i] = 0;
      }
      for (int i = 0; i < cache->Head().PackageCount; i++)
      {
	 pLiterals[i] = 0;
      }
      for (int i = 0; i < cache->Head().PackageCount; i++)
      {
	 scores[i] = 0;
      }
   }

   Clasp::Literal translate(pkgCache::Version *rawVer)
   {
      if (literals[rawVer->ID] == 0)
      {
	 literals[rawVer->ID] = ++literalCount;
	 literalsReverse[literalCount] = rawVer;
      }
      return Clasp::toLit(literals[rawVer->ID]);
   }
   Clasp::Literal translate(pkgCache::Package *rawPkg)
   {
      if (pLiterals[rawPkg->ID] == 0)
      {
	 pLiterals[rawPkg->ID] = ++literalCount;
      }
      return Clasp::toLit(pLiterals[rawPkg->ID]);
   }

   Clasp::Literal relaxationLiteral()
   {
      auto var = ++literalCount;
      relaxLiterals.push_back(var);
      return Clasp::toLit(var);
   }

   bool isSatisfiedNow(pkgCache::DepIterator d)
   {
      for (;;)
      {
	 std::unique_ptr<pkgCache::Version *[]> all(d.AllTargets());

	 for (int i = 0; all[i]; i++)
	 {
	    pkgCache::VerIterator ver(cache, all[i]);
	    pkgCache::PkgIterator pkg = ver.ParentPkg();

	    if (pkg->CurrentVer && pkg.CurrentVer()->ID == ver->ID)
	       return true;
	 }

	 if ((d->CompareOp & pkgCache::Dep::Or) == 0)
	    break;
	 d++;
      }

      return false;
   }

   pkgCache::DepIterator findOldDepends(pkgCache::VerIterator cur, pkgCache::DepIterator dep)
   {
      for (auto d = cur.DependsList(); d.end() == false; ++d)
      {
	 if (d.TargetPkg() == dep.TargetPkg())
	 {
	    return d;
	 }
      }
      return pkgCache::DepIterator();
   }

   void initRules()
   {
      std::queue<pkgCache::Version *> versionsQueue;
      pkgDepCache *depcache = cache;
      for (auto pkg = depcache->PkgBegin(); !pkg.end(); pkg++)
      {
	 if (cache[pkg].Keep() && pkg->CurrentVer == 0)
	    continue;

	 for (auto ver = pkg.VersionList(); !ver.end(); ver++)
	    versionsQueue.push(ver);
      }

      while (!versionsQueue.empty())
      {
	 auto rawVer = versionsQueue.front();
	 versionsQueue.pop();

	 if (versions.find(rawVer) != versions.end())
	    continue;

	 versions.insert(rawVer);

	 pkgCache::VerIterator ver(cache, rawVer);

	 for (auto dep = ver.DependsList(); !dep.end(); dep++)
	 {
	    switch (dep->Type)
	    {
	    case pkgCache::Dep::Recommends:
	    {
	       auto cur = ver.ParentPkg().CurrentVer();
	       // If the package has been installed before, check if the
	       // recommends is (a) new or (b) satisfied before
	       if (!cur.end())
	       {
		  auto oldDep = findOldDepends(cur, dep);
		  if (!oldDep.end() && !isSatisfiedNow(oldDep))
		     break;
	       }
	       // Fall through
	    }
	    case pkgCache::Dep::Depends:
	    case pkgCache::Dep::PreDepends:
	    {
	       Rule rule{{}, Op::GE, 0};
	       int count = 0;

	       for (;;)
	       {
		  std::unique_ptr<pkgCache::Version *[]> targets(dep.AllTargets());
		  int i;
		  for (i = 0; targets[i] != nullptr; i++)
		  {
		  }

		  std::sort(targets.get(), targets.get() + i, CompareProviders(dep));
		  for (int i = 0; targets[i] != nullptr; i++)
		  {
		     // Choice 0 gets 10^0, other choices i get 10^(k+1)
		     // which means the second alternative is 100 times
		     // more expensive than the first one, but further
		     // ones each just 10 times the one before it.
		     int score = 1;
		     for (int j = 0; j < count && score < 100000; j++)
			score *= (j == 0) ? 100 : 10;

		     scores[pkgCache::VerIterator(cache, targets[i]).ParentPkg()->ID] += score;
		     rule.lhs.push_back(WeightedLit{translate(targets[i]), 1});
		     versionsQueue.push(targets[i]);
		     count++;
		  }
		  if ((dep->CompareOp & pkgCache::Dep::Or) == 0)
		     break;
		  dep++;
	       }

	       if (dep->Type == pkgCache::Dep::Recommends)
		  rule.lhs.push_back(WeightedLit{relaxationLiteral(), 1});
	       rule.lhs.push_back(WeightedLit{translate(ver), -1});
	       rules.push_back(rule);

	       break;
	    }
	    case pkgCache::Dep::DpkgBreaks:
	    case pkgCache::Dep::Conflicts:
	    {
	       //std::cout << "Package " << pkg.Name() << " version " << ver.VerStr() << " has conflict " << dep << std::endl;
	       Rule rule{{}, Op::GE, -1};
	       int count = 0;
	       auto type = dep->Type;

	       for (;;)
	       {
		  std::unique_ptr<pkgCache::Version *[]> targets(dep.AllTargets());
		  for (int i = 0; targets[i] != nullptr; i++)
		  {
		     rule.lhs.push_back(WeightedLit{translate(targets[i]), -1});
		     versionsQueue.push(targets[i]);
		     count++;
		  }

		  auto nextDep = dep;
		  nextDep++;
		  if (nextDep.end() || nextDep->Type != dep->Type)
		     break;

		  dep = nextDep;
	       }
	       if (count > 0)
	       {
		  rule.lhs.push_back(WeightedLit{translate(ver), -count});
		  rule.rhs = -count;
		  rules.push_back(rule);
	       }
	       break;
	    }
	    }
	    }
      }

      std::unordered_set<int> seenPackages;
      for (auto rawVer : versions)
      {
	 pkgCache::VerIterator ver(cache, rawVer);
	 pkgCache::PkgIterator pkg = ver.ParentPkg();
	 if (seenPackages.find(pkg->ID) != seenPackages.end())
	    continue;

	 seenPackages.insert(pkg->ID);

	 Rule rule{{}, Op::EQ, 0};
	 for (auto ver = pkg.VersionList(); !ver.end(); ver++)
	 {
	    if (versions.find(ver) != versions.end())
	       rule.lhs.push_back(WeightedLit{translate(ver), 1});
	 }
	 rule.lhs.push_back(WeightedLit{translate(pkg), -1});
	 rules.push_back(rule);

	 if (_config->FindB("APT::Solver::kalel::Strict-Pinning", _config->FindB("APT::Solver::Strict-Pinning", true)))
	 {
	    Rule strictPinningRule{{}, Op::EQ, 0};
	    for (auto ver = pkg.VersionList(); !ver.end(); ver++)
	    {
	       if (cache->GetCandidateVersion(pkg)->ID == ver->ID)
		  continue;
	       if (pkg->CurrentVer && pkg.CurrentVer()->ID == ver->ID)
		  continue;

	       if (versions.find(ver) != versions.end())
		  strictPinningRule.lhs.push_back(WeightedLit{translate(ver), 1});
	    }
	    if (!strictPinningRule.lhs.empty())
	       rules.push_back(strictPinningRule);
	 }
      }
   }

   int countVariables()
   {
      return literalCount;
   }

   /* min: -installed packages */
   void setCriterion(std::string const &criterion)
   {
      objective.clear();
      if (criterion == "-removed")
      {
	 for (auto pkg = cache->PkgBegin(); !pkg.end(); pkg++)
	 {
	    if (pkg->CurrentVer)
	       objective.push_back(WeightedLit{translate(pkg), -1});
	 }
      }
      else if (criterion == "-notuptodate")
      {
	 for (auto pkg = cache->PkgBegin(); !pkg.end(); pkg++)
	 {
	    auto cand = cache->GetCandidateVersion(pkg);
	    if (pkg->CurrentVer && cand)
	       objective.push_back(WeightedLit{translate(cand), -1});
	 }
      }
      else if (criterion == "-changed")
      {
	 for (auto rawVer : versions)
	 {
	    pkgCache::VerIterator ver(cache, rawVer);
	    if (!ver.ParentPkg().CurrentVer().end() && ver.ParentPkg().CurrentVer()->ID == rawVer->ID)
	       objective.push_back(WeightedLit{translate(rawVer), -1});
	 }
      }
      else if (criterion == "-ordered_new")
      {
	 for (auto pkg = cache->PkgBegin(); !pkg.end(); pkg++)
	 {
	    if (pLiterals[pkg->ID] != 0 && pkg->CurrentVer == 0)
	       objective.push_back(WeightedLit{translate(pkg), scores[pkg->ID] ?: 1});
	 }
      }
      else if (criterion == "-new")
      {
	 for (auto pkg = cache->PkgBegin(); !pkg.end(); pkg++)
	 {
	    if (pLiterals[pkg->ID] != 0 && pkg->CurrentVer == 0)
	       objective.push_back(WeightedLit{translate(pkg), 1});
	 }
      }
      else if (criterion == "-newunsatrecommends")
      {
	 for (auto var : relaxLiterals)
	 {
	    objective.push_back(WeightedLit{Clasp::posLit(var), 1});
	 }
      }
      else
      {
	 std::cerr << "Invalid criterion: " << criterion << std::endl;
      }
   }

   void solve(bool report = false)
   {
      // Create copies
      auto rules = this->rules;
      auto objective = this->objective;
      Clasp::ClaspFacade clasp;
      Clasp::ClaspConfig config;
      //config.solve.enumMode  = Clasp::EnumOptions::enum_user;
      //config.solve.numModels = 0;
      //Clasp::OptParams &params = config.solver(0).opt;

      config.addSolver(0).opt.type = Clasp::OptParams::type_usc;
      //config.addSolver(0).opt.algo = Clasp::OptParams::usc_oll;

      //sparams = Clasp::OptParams(Clasp::OptParams::type_usc);

      Clasp::PBBuilder &pb = clasp.startPB(config);

      pb.prepareProblem(countVariables(), 0, 0, rules.size());

      if (objective.size() != 0)
      {
	 std::cerr << "Adding objective\n";
	 pb.addObjective(objective);
      }

      for (auto pkg = cache->PkgBegin(); !pkg.end(); pkg++)
      {
	 if (cache[pkg].Install())
	 {
	    WeightedLit lit{translate(cache[pkg].InstVerIter(cache)), 1};
	    WeightedVec vec;
	    vec.push_back(lit);
	    pb.addConstraint(vec, 1, true);
	 }
	 if (cache[pkg].Delete())
	 {
	    WeightedLit lit{translate(pkg), 1};
	    WeightedVec vec;
	    vec.push_back(lit);
	    pb.addConstraint(vec, 0, true);
	 }
      }

      for (auto &rule : rules)
      {
	 pb.addConstraint(rule.lhs, rule.rhs, rule.op == Op::EQ);
      }

      int count = 0;
      for (Clasp::ClaspFacade::SolveHandle h = clasp.solve(Clasp::SolveMode_t::Yield); h.next();)
      {
	 // print the model
	 std::cerr << "Found a model\n";

	 //printModel(clasp.ctx.output, *h.model());
	 int count = 0;

	 for (auto &wlit : objective)
	 {

	    if (h.model()->isTrue(wlit.first))
	    {
	       count += wlit.second;
	    }
	 }

#if 0
			std::cerr << "v";
			for (auto rawVer : versions) {
				pkgCache::VerIterator v(cache, rawVer);
				bool toBe = h.model()->isTrue(translate(v));

				if (!toBe) {
					std::cerr << " -x" << translate(v).var();
				} else {
					std::cerr << " x" << translate(v).var();
				}
				if (translate(v).var() % 19 == 0 && translate(v).var() != 0)
					std::cerr << "\nv";

			}
			std::cerr << "\n";
#endif
	 if (report)
	    reportResult(h);

	 std::cerr << "\no " << count << std::endl;

	 this->rules.push_back(Rule{this->objective, Op::EQ, count});

	 // exclude this model
      }
      std::cerr << clasp.result() << std::endl;
      if (clasp.solving())
	 abort();
      std::cerr << clasp.result() << std::endl;
   }

   void reportResult(Clasp::ClaspFacade::SolveHandle &h)
   {

      pkgDepCache::ActionGroup g(cache);
      std::unique_ptr<pkgCache::Version *[]> newState(new pkgCache::Version *[literalCount + 1]);
      for (int i = 0; i < literalCount + 1; i++)
	 newState[i] = nullptr;
      for (auto rawVer : versions)
      {
	 pkgCache::VerIterator v(cache, rawVer);
	 bool toBe = h.model()->isTrue(translate(v));

	 assert(pLiterals[v.ParentPkg()->ID] != 0);

	 if (toBe)
	    newState[translate(v.ParentPkg()).var()] = v;
      }

      int upgrade = 0;
      int install = 0;
      int remove = 0;
      int other = 0;
      for (auto pkg = cache->PkgBegin(); !pkg.end(); pkg++)
      {
	 if (pLiterals[pkg->ID] == 0)
	    continue;
	 auto oldS = pkg.CurrentVer();
	 auto newS = pkgCache::VerIterator(cache, newState[translate(pkg).var()]);

	 if (newS.end() && oldS.end())
	    ;
	 else if (!oldS.end() && !newS.end())
	 {
	    if (newS->ID != oldS->ID)
	    {
	       std::cerr << "Upgrading " << pkg.FullName(true) << " " << oldS.VerStr() << " -> " << newS.VerStr() << "\n";
	       upgrade++;
	       cache->SetCandidateVersion(newS);
	       cache->MarkInstall(pkg, false);
	    }
	 }
	 else if (oldS.end() && !newS.end())
	 {
	    std::cerr << "Installing " << pkg.FullName(true) << " " << newS.VerStr() << "\n";
	    install++;
	    cache->SetCandidateVersion(newS);
	    cache->MarkInstall(pkg, false);
	 }
	 else if (!oldS.end() && newS.end())
	 {
	    std::cerr << "Deleting " << pkg.FullName(true) << " " << oldS.VerStr() << "\n";
	    cache->MarkDelete(pkg);
	    remove++;
	 }
	 else
	 {
	    std::cerr << "Unknown state " << pkg.FullName(true) << " " << oldS.VerStr() << " -> " << newS.VerStr() << "\n";
	    other++;
	 }
      }
      std::cerr << upgrade << " upgraded, " << install << " newly installed, " << remove << " to remove"
		<< ".\n";
   }

   void dump()
   {
      std::cout << "* #variable= " << countVariables() << " #constraint= " << rules.size() << "\n";

      if (objective.size() != 0)
      {
	 std::cout << "min:";
	 bool first = true;
	 for (auto &wlit : objective)
	 {
	    if (!first)
	       std::cout << " ";
	    std::cout << wlit.second << " x" << wlit.first.var();
	    first = false;
	 }
	 std::cout << ";\n";
      }

      for (auto pkg = cache->PkgBegin(); !pkg.end(); pkg++)
      {
	 if (cache[pkg].Install())
	 {
	    std::cerr << pkg.Name() << std::endl;
	    std::cout << " 1 x" << literals[cache[pkg].InstVerIter(cache)->ID];
	 }
      }

      std::cout << ">= 1;\n";

      for (auto &rule : rules)
      {
	 bool first = true;
	 for (auto &wlit : rule.lhs)
	 {
	    if (!first)
	       std::cout << " ";
	    std::cout << wlit.second << " x" << wlit.first.var();
	    first = false;
	 }
	 if (rule.op == Op::GE)
	    std::cout << " >= ";
	 if (rule.op == Op::EQ)
	    std::cout << " = ";

	 std::cout << rule.rhs << ";\n";
      }
   }

   std::vector<WeightedVec> generatePinningObjectives()
   {
      // Our requirements are:
      // 1. Keep downgrades away
      // 2. Keep upgrades away by priority
      size_t objectiveCount = 0;
      std::vector<std::vector<pkgCache::VerIterator>> perPackage;
      pkgPolicy *policy = cache;

      // Build a vector of package versions per package that is
      // ordered.
      for (auto pkg = cache->PkgBegin(); !pkg.end(); pkg++)
      {
	 std::vector<pkgCache::VerIterator> packageVersions;
	 pkgCache::VerIterator cur = pkg.CurrentVer();
	 pkgCache::VerIterator can = cache->GetCandidateVersion(pkg);

	 if (pLiterals[pkg->ID] == 0)
	    continue;

	 for (auto ver = pkg.VersionList(); !ver.end(); ver++)
	 {
	    if (literals[ver->ID] == 0 || (!cur.end() && cur->ID == ver->ID) || (!can.end() && can->ID == ver->ID))
	       continue;

	    packageVersions.push_back(ver);
	 }

	 std::sort(packageVersions.begin(), packageVersions.end(), [policy, cur](pkgCache::VerIterator a, pkgCache::VerIterator b) {
	    // A is a downgrade, B not; so A has higher cost
	    if (!cur.end() && (a.CompareVer(cur) < 0) && (b.CompareVer(cur) >= 0))
	       return true;

	    // A has higher priority so comes later.
	    if (policy->GetPriority(a) > policy->GetPriority(b))
	       return false;

	    return a.CompareVer(b) <= 0;
	 });

	 if (packageVersions.size() > objectiveCount)
	    objectiveCount = packageVersions.size();

	 perPackage.push_back(packageVersions);
      }

      // Bui
      std::vector<WeightedVec> objectives;
      for (int i = 0; i < objectiveCount; i++)
      {
	 WeightedVec vector;

	 for (auto &perPackageVec : perPackage)
	 {
	    if (perPackageVec.size() > i)
	       vector.push_back(WeightedLit{translate(perPackageVec[i]), 1});
	 }

	 objectives.push_back(vector);
      }

      return objectives;
   }

   void fixPinning()
   {
      if (_config->FindB("APT::Solver::kalel::Strict-Pinning", _config->FindB("APT::Solver::Strict-Pinning", true)) == false)
      {
	 for (auto &obj : generatePinningObjectives())
	 {
	    objective = obj;
	    solve();
	 }
      }
   }
};

#if 0
int main(int argc, char *argv[])
{
   pkgInitConfig(*_config);
   pkgInitSystem(*_config, _system);
   pkgCacheFile cache;

   cache.Open(nullptr, false);

   std::clog << "Broken count:" << cache->BrokenCount() << "\n";
   std::clog << "Inst count:" << cache->InstCount() << "\n";
   Solver solver(cache);

   auto verset = APT::VersionSet::FromCommandLine(cache, (const char **)argv + 1);
   {
      pkgDepCache::ActionGroup g(cache);
      for (auto &ver : verset) {
	 cache->SetCandidateVersion(ver);
	 cache->MarkInstall(ver.ParentPkg(), false);
      }
   }

   solver.initRules();

   if (argv[1] == std::string("upgrade"))
   {
      std::cerr << "upgrade\n";
      solver.setCriterion("-newunsatrecommends");
      solver.solve();
      solver.setCriterion("-removed");
      solver.solve();
      solver.fixPinning();
      solver.setCriterion("-ordered_new");
      solver.solve();
      solver.setCriterion("-notuptodate");
      solver.solve(true);
   }
   else if (argv[1] == std::string("new-upgrade"))
   {
      std::cerr << "upgrade\n";
      solver.setCriterion("-newunsatrecommends");
      solver.solve();
      solver.setCriterion("-removed");
      solver.solve();
      solver.setCriterion("-notuptodate");
      solver.solve();
      solver.fixPinning();
      solver.setCriterion("-ordered_new");
      solver.solve(true);
   }
   else if (argv[1] == std::string("dist-upgrade"))
   {
      std::cerr << "dist-upgrade\n";
      solver.setCriterion("-newunsatrecommends");
      solver.solve();
      solver.setCriterion("-notuptodate");
      solver.solve();
      solver.fixPinning();
      solver.setCriterion("-ordered_new");
      solver.solve(true);
   }
   else if (argv[1] == std::string("install"))
   {

      solver.setCriterion("-newunsatrecommends");
      solver.solve();
      std::cerr << "install: changed\n";
      solver.setCriterion("-changed");
      solver.solve();
      solver.setCriterion("-removed");
      solver.solve();
      std::cerr << "install: new\n";
      solver.fixPinning();
      solver.setCriterion("-ordered_new");
      solver.solve(true);
   }

   pkgProblemResolver r(cache);
   {
      pkgDepCache::ActionGroup g(cache);
      for (auto &ver : verset)
	 cache->MarkInstall(ver.ParentPkg(), true);
   }
   for (auto &ver : verset)
      r.Protect(ver.ParentPkg());
   r.Resolve();


   std::clog << "Broken count:" << cache->BrokenCount() << "\n";
   std::clog << "Inst count:" << cache->InstCount() << "\n";
}

#else
// -*- mode: cpp; mode: fold -*-
// Description								/*{{{*/
/* #####################################################################

   cover around the internal solver to be able to run it like an external

   ##################################################################### */
									/*}}}*/
// Include Files							/*{{{*/
#include <apt-pkg/algorithms.h>
#include <apt-pkg/cachefile.h>
#include <apt-pkg/cacheset.h>
#include <apt-pkg/cmndline.h>
#include <apt-pkg/configuration.h>
#include <apt-pkg/depcache.h>
#include <apt-pkg/edsp.h>
#include <apt-pkg/error.h>
#include <apt-pkg/fileutl.h>
#include <apt-pkg/init.h>
#include <apt-pkg/pkgcache.h>
#include <apt-pkg/pkgsystem.h>
#include <apt-pkg/strutl.h>
#include <apt-pkg/upgrade.h>


#include <cstdio>
#include <iostream>
#include <list>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
									/*}}}*/

static bool ShowHelp(CommandLine &)					/*{{{*/
{
	std::cout <<
	       ("Usage: apt-internal-solver\n"
		"\n"
		"apt-internal-solver is an interface to use the current internal\n"
		"resolver for the APT family like an external one, for debugging or\n"
		"the like.\n");
	return true;
}
									/*}}}*/
APT_NORETURN static void DIE(std::string const &message) {		/*{{{*/
	std::cerr << "ERROR: " << message << std::endl;
	_error->DumpErrors(std::cerr);
	exit(EXIT_FAILURE);
}
									/*}}}*/
static bool WriteSolution(pkgDepCache &Cache, FileFd &output)		/*{{{*/
{
   bool Okay = output.Failed() == false;
   for (pkgCache::PkgIterator Pkg = Cache.PkgBegin(); Pkg.end() == false && (Okay); ++Pkg)
   {
      std::string action;
      if (Cache[Pkg].Delete() == true)
	 Okay &= EDSP::WriteSolutionStanza(output, "Remove", Pkg.CurrentVer());
      else if (Cache[Pkg].NewInstall() == true || Cache[Pkg].Upgrade() == true)
	 Okay &= EDSP::WriteSolutionStanza(output, "Install", Cache.GetCandidateVersion(Pkg));
      else if (Cache[Pkg].Garbage == true)
	 Okay &= EDSP::WriteSolutionStanza(output, "Autoremove", Pkg.CurrentVer());
   }
   return Okay;
}
									/*}}}*/
int main(int argc,const char *argv[])					/*{{{*/
{
	if (pkgInitConfig(*_config) == false)
		DIE("System could not be initialized!");

	// we really don't need anything
	DropPrivileges();


	// Deal with stdout not being a tty
	if (!isatty(STDOUT_FILENO) && _config->FindI("quiet", -1) == -1)
		_config->Set("quiet","1");

	if (_config->FindI("quiet", 0) < 1)
		_config->Set("Debug::EDSP::WriteSolution", true);

	_config->Set("APT::System", "Debian APT solver interface");
	_config->Set("APT::Solver", "internal");
	_config->Set("edsp::scenario", "/nonexistent/stdin");
	_config->Clear("Dir::Log");
	FileFd output;
	if (output.OpenDescriptor(STDOUT_FILENO, FileFd::WriteOnly | FileFd::BufferedWrite, true) == false)
	   DIE("stdout couldn't be opened");
	int const input = STDIN_FILENO;
	SetNonBlock(input, false);

	EDSP::WriteProgress(0, "Start up solver…", output);

	if (pkgInitSystem(*_config,_system) == false)
		DIE("System could not be initialized!");

	EDSP::WriteProgress(1, "Read request…", output);

	if (WaitFd(input, false, 5) == false)
		DIE("WAIT timed out in the resolver");

	std::list<std::string> install, remove;
	unsigned int flags;
	if (EDSP::ReadRequest(input, install, remove, flags) == false)
		DIE("Parsing the request failed!");

	EDSP::WriteProgress(5, "Read scenario…", output);

	pkgCacheFile CacheFile;
	if (CacheFile.Open(NULL, false) == false)
		DIE("Failed to open CacheFile!");

	EDSP::WriteProgress(50, "Apply request on scenario…", output);

	if (EDSP::ApplyRequest(install, remove, CacheFile) == false)
		DIE("Failed to apply request to depcache!");


	EDSP::WriteProgress(60, "Call problemresolver on current scenario…", output);

	Solver solver(CacheFile);

	solver.initRules();
	if (flags & EDSP::Request::UPGRADE_ALL) {
	    solver.setCriterion("-newunsatrecommends");
	    solver.solve();
	    if (flags & EDSP::Request::FORBID_REMOVE) {
	       solver.setCriterion("-removed");
	       solver.solve();
	    }
	    if (flags & EDSP::Request::FORBID_NEW_INSTALL) {
	       solver.setCriterion("-ordered_new");
	       solver.solve();
	    }
	    solver.setCriterion("-notuptodate");
	    solver.solve();
	    solver.fixPinning();
	    if (!(flags & EDSP::Request::FORBID_NEW_INSTALL)) {
	       solver.setCriterion("-ordered_new");
	       solver.solve();
	    }
	    solver.solve(true);
	} else {
	    solver.setCriterion("-newunsatrecommends");
	    solver.solve();
	    std::cerr << "install: changed\n";
	    solver.setCriterion("-changed");
	    solver.solve();
	    solver.setCriterion("-removed");
	    solver.solve();
	    std::cerr << "install: new\n";
	    solver.fixPinning();
	    solver.setCriterion("-ordered_new");
	    solver.solve(true);
        }


	EDSP::WriteProgress(95, "Write solution…", output);

	if (WriteSolution(CacheFile, output) == false)
		DIE("Failed to output the solution!");

	EDSP::WriteProgress(100, "Done", output);

	return 0;
}
									/*}}}*/
#endif
