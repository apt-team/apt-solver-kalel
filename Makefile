apt-solver-kalel: apt-solver-kalel.cc
	$(CXX) $(CXXFLAGS) -I /usr/include/potassco-*/ -I /usr/include/clasp-*/ -O2 -g -o $@ $^ $(LDLIBS) -lapt-pkg -L /usr/lib/x86_64-linux-gnu/clasp-*/ -lclasp -L /usr/lib/x86_64-linux-gnu/potassco-*/ -lpotassco -pthread
install: apt-solver-kalel
	install -d $(DESTDIR)/usr/lib/apt/solvers
	install -m755 apt-solver-kalel $(DESTDIR)/usr/lib/apt/solvers/kalel

clean:
	rm -f apt-solver-kalel
